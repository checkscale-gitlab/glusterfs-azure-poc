resource "azurerm_virtual_network" "main" {
  name                = "${var.name-prefix}main"
  address_space       = [var.network-cidr]
  location            = data.azurerm_resource_group.main-rc.location
  resource_group_name = data.azurerm_resource_group.main-rc.name
}

resource "azurerm_subnet" "main-sub" {
  name                 = "${var.name-prefix}internal-01"
  resource_group_name  = data.azurerm_resource_group.main-rc.name
  virtual_network_name = azurerm_virtual_network.main.name
  address_prefixes     = [var.subnet-cidr]
}

resource "azurerm_public_ip" "bastion-vm-ip" {
  name                = "${var.name-prefix}bastion-ip"
  location            = data.azurerm_resource_group.main-rc.location
  resource_group_name = data.azurerm_resource_group.main-rc.name
  allocation_method   = "Dynamic"
  sku                 = "Basic"
}

resource "azurerm_network_interface" "bastion-nic" {
  name                = "${var.name-prefix}nic-bh"
  resource_group_name = data.azurerm_resource_group.main-rc.name
  location            = data.azurerm_resource_group.main-rc.location

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.main-sub.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.bastion-vm-ip.id
  }
}

resource "azurerm_network_interface" "gluster-nic" {
  count               = var.cluster-size
  name                = "${var.name-prefix}nic-gluster${count.index + 1}"
  resource_group_name = data.azurerm_resource_group.main-rc.name
  location            = data.azurerm_resource_group.main-rc.location

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.main-sub.id
    private_ip_address_allocation = "Static"
    private_ip_address            = cidrhost(azurerm_subnet.main-sub.address_prefixes[0], count.index + 10)
  }
}
