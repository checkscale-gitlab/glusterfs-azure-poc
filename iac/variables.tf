variable "name-prefix" {
  type    = string
  default = "glusterfs-demo-"
}

variable "rg-name" {
  type    = string
  default = "glusterfs-demo"
}

variable "cluster-size" {
  type        = number
  description = "Size of GlusterFS cluster"
  default     = 3
}

variable "network-cidr" {
  type        = string
  description = "CIDR block for the main network"
  default     = "10.0.0.0/16"
}

variable "subnet-cidr" {
  type        = string
  description = "CIDR block for the main subnet"
  default     = "10.0.1.0/24"
}
