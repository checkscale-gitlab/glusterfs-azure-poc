terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>2.0"

    }
  }

  backend "http" {
  }
}

provider "azurerm" {
  features {}
}

data "azurerm_resource_group" "main-rc" {
  name = var.rg-name
}
