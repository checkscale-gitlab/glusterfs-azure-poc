#! /bin/bash

add-apt-repository ppa:/gluster/glusterfs-7
apt update && apt install glusterfs-server -y
systemctl enable glusterd