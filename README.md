[![Gitlab pipeline status](https://gitlab.com/ollaww/glusterfs-azure-poc/badges/main/pipeline.svg)](https://gitlab.com/ollaww/glusterfs-azure-poc/commits/main)
# glusterfs-azure-poc

Demo project in which a cluster of Azure VMs are used to create a [GlusterFS](https://docs.gluster.org/en/latest/).

## Tools involved
- [Terraform](https://www.terraform.io/) to deploy the infrastructure on [Azure](https://azure.microsoft.com/en-us/)
- [Ansible](https://www.ansible.com/) used from a Bastion host to setup GlusterFS on virtual machines.

## Setup

1. Create a [ResourceGroup](https://docs.microsoft.com/en-us/azure/azure-resource-manager/management/manage-resource-groups-portal) on Azure and generate valid credentials to create resources ;
2. [Setup Terraform to interact with Azure](https://docs.microsoft.com/en-us/azure/developer/terraform/overview) ;
3. Generate RSA keys:
    1. First one is used to connect to BH (`ssh-keygen -f iac/ssh/bastion_rsa`) ;
    2. Second one is used by Ansible to connect to the cluster VMs (`ssh-keygen -f iac/ssh/ansble_rsa`) ;
4. Edit terraform variables if needed (`iac/terraform.tfvars`) ;
4. Deploy the infrastructure with Terraform:
   1. Move to `iac` directory and run `terraform init` to initialize Terraform.
   2. Run `terraform plan` to have a preview on what will be created.
   3. Run `terraform apply` to create the stack.
5. Ansible - WIP
